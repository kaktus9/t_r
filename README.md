# README #


Brief Manual

All sources need to be Cross Compiled to MIPS arch.

Any viable Cross Compilation toolchain may be used.

All configuration and control scripts are already included and configured within the source.

ALERT: before compiling the sources gettext patch needs to be applied to kernel makefile and other makefiles if needed (Make will notify and abort make process during compilation if that is the case).

Specific submodules should be compiled automatically after invoking make in root directory. 

Successful compilation will yield a binary .chk file ready do be uploaded to the WNDR4500 device. 

Other MIPS - compatible devices may be used after modifying hardware-dependent sources and makefiles as required.

Control scripts and binaries are located within "prebuilt" subdirectory. (/ROUTER/WNDR4500-V1.0.1.20_1.0.40_src/src/prebuilt/WW)

Below default toolchain (included within downloads section of this repository) install instructions are included:

To install the toolchain

(1) The toolchain MUST be installed under /projects/hnd/tools/linux/ directory. After mkdir the dir,

        tar xvfz hndtools-mipsel-linux-uclibc-4.2.3.tgz

(2) Add the path

        export PATH=/projects/hnd/tools/linux/hndtools-mipsel-linux-uclibc-4.2.3/bin:$PATH

   and the do the following to verify the toolchain

        mipsel-uclibc-linux26-gcc --ver

If using another hardware platform recompilation of those should not be required.

CGI server (boa) startup  should be included within device's  master initialization sequence (init.d or equivalent)  Rest of the modules are loaded after receiving trigger via web interface and handled automatically.

the catalog structure within prebuilt should not be changed or the scripts may not initialize properly.

from the script's "point of view" they are executed on a embedded linux Busy Box instance. thus relative paths are being used within the scripts.

The project status is GO (fully operational). 

Fully operational .chk binary for the NETGEAR WNDR4500 platform is included within the downloads section of this repository for Your convenience. As stated before, if using another hardware platform source alteration is required, however if the used platform supports ELF 32-bit LSB executable, MIPS, MIPS32 version 1 binary format recompilation of embedded custom binaries should not be required.

Final note: for Your convenience it is every useful to set the following variables within .bashrc of the user account under which the binary is compiled (most probably root via sudo)

 export cross=mipsel-linux-uclibc-

 export CC="${cross}gcc"

 export RANLIB="${cross}ranlib"

While compiling openssl be sure to provide the following options to Configure (note: for openssl it is capital C )

--prefix=/home/kaktus9/Desktop/SRC/TOR/CC/NEW_TOR/out_deps/openssl dist threads -D_REENTRANT

Use the following addresses to check out the code:

HTTPS:

$ git clone https://kaktus9@bitbucket.org/kaktus9/t_r.git

SSH:

$ git clone git@bitbucket.org:kaktus9/t_r.git

In case of questions You can email me at kaktus9@gmail.com

Where possible the code is POSIX compliant so any standard compiler with correct MIPS toolchains should not complain. 

As stated above revisions as to file placement and permissions may be necessary dependent on the platform the solution is deployed to. (reminder: the compiled binaries were deployed on NETGEAR WNDR4500 because it was one of the few routers with both 128MB of memory and a MIPS core at the time of initial development and testing back in the early 2014.)

README Updated 2016-06-24