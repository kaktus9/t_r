/*
 * Export MIPS-specific functions needed for loadable modules.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 *
 * Copyright (C) 1996, 97, 98, 99, 2000, 01, 03, 04, 05 by Ralf Baechle
 * Copyright (C) 1999, 2000, 01 Silicon Graphics, Inc.
 */
#include <linux/interrupt.h>
#include <linux/module.h>
#include <asm/checksum.h>
#include <asm/pgtable.h>
#include <asm/uaccess.h>

extern void *__bzero(void *__s, size_t __count);
extern long __strncpy_from_user_nocheck_asm(char *__to,
                                            const char *__from, long __len);
extern long __strncpy_from_user_asm(char *__to, const char *__from,
                                    long __len);
extern long __strlen_user_nocheck_asm(const char *s);
extern long __strlen_user_asm(const char *s);
extern long __strnlen_user_nocheck_asm(const char *s);
extern long __strnlen_user_asm(const char *s);

/*
 * String functions
 */
EXPORT_SYMBOL(memset);
EXPORT_SYMBOL(memcpy);
EXPORT_SYMBOL(memmove);

EXPORT_SYMBOL(kernel_thread);

/*
 * Userspace access stuff.
 */
EXPORT_SYMBOL(__copy_user);
EXPORT_SYMBOL(__copy_user_inatomic);
EXPORT_SYMBOL(__bzero);
EXPORT_SYMBOL(__strncpy_from_user_nocheck_asm);
EXPORT_SYMBOL(__strncpy_from_user_asm);
EXPORT_SYMBOL(__strlen_user_nocheck_asm);
EXPORT_SYMBOL(__strlen_user_asm);
EXPORT_SYMBOL(__strnlen_user_nocheck_asm);
EXPORT_SYMBOL(__strnlen_user_asm);

EXPORT_SYMBOL(csum_partial);
EXPORT_SYMBOL(csum_partial_copy_nocheck);
EXPORT_SYMBOL(__csum_partial_copy_user);

EXPORT_SYMBOL(invalid_pte_table);
/* Fxcn port-S Wins, 0714-09 */
/*
 * Kernel hacking ...
 */
//Foxconn add start, Lewis Min, for OpenDNS, 12/12/2008
extern void insert_func_to_BR_PRE_ROUTE(void *FUNC);
EXPORT_SYMBOL(insert_func_to_BR_PRE_ROUTE);
extern void insert_func_to_BR_POST_ROUTE(void *FUNC);
EXPORT_SYMBOL(insert_func_to_BR_POST_ROUTE);
extern void remove_func_from_BR_POST_ROUTE(void);
EXPORT_SYMBOL(remove_func_from_BR_POST_ROUTE);
extern void remove_func_from_BR_PRE_ROUTE(void);
EXPORT_SYMBOL(remove_func_from_BR_PRE_ROUTE);
//Foxconn add end, Lewis Min, for OpenDNS, 12/12/2008

/* Foxconn add start, Zz Shan@MutiSsidControl 03/13/2009*/
#ifdef MULTIPLE_SSID
extern void insert_msc_func_to_br(void *FUNC);
EXPORT_SYMBOL(insert_msc_func_to_br);
extern void remove_msc_func_from_br();
EXPORT_SYMBOL(remove_msc_func_from_br);

/*foxconn modified start, water, 01/07/10*/
//#include "../../../router/multissidcontrol/MultiSsidControl.h"
#include "../../../../../../ap/acos/multissidcontrol/MultiSsidControl.h"
/*foxconn modified end, water, 01/07/10*/
extern T_MSsidCtlProfile *gProfile;
EXPORT_SYMBOL(gProfile);
extern int gProfilenum;
EXPORT_SYMBOL(gProfilenum);
/* Foxconn added start pling 10/06/2010 */
extern T_MSsidCtlProfile *gProfile_5g;
EXPORT_SYMBOL(gProfile_5g);
extern int gProfilenum_5g;
EXPORT_SYMBOL(gProfilenum_5g);
/* Foxconn added end pling 10/06/2010 */
#endif
/* Foxconn add end, Zz Shan 03/13/2009*/
/* Fxcn port-E Wins, 0714-09 */
