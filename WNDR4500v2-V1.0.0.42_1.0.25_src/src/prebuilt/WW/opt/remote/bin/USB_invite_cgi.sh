#!/bin/sh
#-------------------------------------------------------------------------
#  Copyright 2011, Foxconn
#  All rights reserved.
#  Author: Perry Liao
#-------------------------------------------------------------------------
# arg: <action{register|unregister}> <user.name> <user.pass>
#-------------------------------------------------------------------------

# load environment
. /opt/remote/bin/comm.sh
. /opt/remote/bin/env.sh



####################################
action="${1}"
username="${2}"
password="${3}"

case "${action}" in
    "register")
		 do_register "$username" "$password" && {
			if [ "xSUCCESS" == "x$COMM_RESULT" ]; then
				$nvram set leafp2p_remote_register="reg_ok"
                return $OK
			fi        
        }    
        $nvram set leafp2p_remote_register="reg_fail"
    ;;
    "unregister")
		do_unregister "$username" "$password" && {
			if [ "xSUCCESS" == "x$COMM_RESULT" ]; then
				$nvram set leafp2p_remote_register="unreg_ok"
                return $OK
			fi   
        }
        $nvram set leafp2p_remote_register="unreg_fail"
    ;;
esac
