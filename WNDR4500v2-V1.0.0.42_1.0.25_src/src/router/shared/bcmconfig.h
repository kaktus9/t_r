/*
 * Automatically generated C config: don't edit
 */
#undef __CONFIG_NAT__

/*
 * Base Features
 */
#define __CONFIG_RC__ 1
#define __CONFIG_NVRAM__ 1
#define __CONFIG_NVRAM_UTILITY__ 1
#define __CONFIG_SHARED__ 1
#define __CONFIG_LIBBCM__ 1
#define __CONFIG_BUSYBOX__ 1
#define __CONFIG_BUSYBOX_CONFIG__ "router"
#define __CONFIG_WLCONF__ 1
#define __CONFIG_BRIDGE__ 1
#define __CONFIG_VLAN__ 1
#undef __CONFIG_HTTPD__
#undef __CONFIG_GLIBC__
#define __CONFIG_UCLIBC__ 1
#define __CONFIG_LIBOPT__ 1
#define __CONFIG_USBAP__ 1
#define __CONFIG_SQUASHFS__ 1
#undef __CONFIG_CRAMFS__

/*
 * Options
 */
#define __CONFIG_VENDOR__ "broadcom"
#undef __CONFIG_UDHCPD__
#define __CONFIG_LIBUPNP__ 1
#undef __CONFIG_SAMBA__
#undef __CONFIG_FFMPEG__

/*
 * DLNA
 */
#undef __CONFIG_DLNA_DMS__
#undef __CONFIG_DLNA_DMR__
#define __CONFIG_NAS__ 1
#undef __CONFIG_WAPI__
#undef __CONFIG_WAPI_IAS__
#define __CONFIG_SES__ 1
#define __CONFIG_SES_CL__ 1
#define __CONFIG_WSCCMD__ 1
#define __CONFIG_WFI__ 1
#define __CONFIG_LLD2D__ 1
#undef __CONFIG_NTP__
#define __CONFIG_UTILS__ 1
#undef __CONFIG_ETC__
#define __CONFIG_BCMWPA2__ 1
#undef __CONFIG_WCN__
#define __CONFIG_EMF__ 1
#undef __CONFIG_WL_ACI__
#undef __CONFIG_MEDIA_IPTV__
#define __CONFIG_PHYMON_UTILITY__ 1
#undef __CONFIG_SHRINK_MEMORY__
#undef __CONFIG_BCMDCS__
#undef __CONFIG_EXTACS__
#undef __CONFIG_IPV6__
#undef __CONFIG_VOIP__
#undef __CONFIG_LIBZ__
#undef __CONFIG_SOUND__

/*
 * Additional C libraries
 */
#define __CONFIG_LIBCRYPT__ 1
#define __CONFIG_LIBDL__ 1
#define __CONFIG_LIBM__ 1
#undef __CONFIG_LIBNSL__
#define __CONFIG_LIBPTHREAD__ 1
#define __CONFIG_LIBRESOLV__ 1
#define __CONFIG_LIBUTIL__ 1

/*
 * Environment
 */
#define __PLATFORM__ "mipsel"
#define __LINUXDIR__ "$(SRCBASE)/linux/linux-2.6"
#define __LIBDIR__ "$(TOOLCHAIN)/lib"
#define __USRLIBDIR__ "$(TOOLCHAIN)/usr/lib"
#undef __CONFIG_RTR_OPTIMIZE_SIZE__

/*
 * Internal Options
 */

/*
 * Advanced Router Features
 */
#define __CONFIG_BCMQOS__ 1
