# This is a generated file.  Do not edit.

package configurehelp;

use strict;
use warnings;
use Exporter;

use vars qw(
    @ISA
    @EXPORT_OK
    $Cpreprocessor
    );

@ISA = qw(Exporter);

@EXPORT_OK = qw(
    $Cpreprocessor
    );

$Cpreprocessor = 'mipsel-linux-uclibc-gcc -E -isystem /root/Desktop/SRC/WNDR4500v2-V1.0.0.42_1.0.25_src/src/router/../../ap/gpl/openssl/tmp/usr/local/ssl/include -isystem /root/Desktop/SRC/WNDR4500v2-V1.0.0.42_1.0.25_src/src/router/../../ap/gpl/openssl/tmp/usr/local/ssl/include/openssl';

1;
