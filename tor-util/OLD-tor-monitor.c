//Marcin Kowalczyk 16.03.2014 Tor monitor executable for any MIPS I  capable system 
//this program replaces old cumbersome init.d shellscript. Along with this iteration tor-enable,tor-disable,tor-preinit,tor-reboot are also being revised do make sure You are using the newest executables as they are NOT backwards compatible. Sorry, but to go forward you must le//ave the past behind
//thus only valid binaries now are tor-preinit,tor-monitor,tor-enable,tor-disable
//manually invoke ONLY tor-enable and tor-disable
#include<stdio.h>
#include <unistd.h> 
#include <stdbool.h> //root check
//#define SLEEP_TIME_DBG 21600 //debug constant to manipulate program sleep time
int main(){
int debug=1;
int ckerror=-9; //result check variable 
while (true){
//sleep
//delete post debug
if(debug==1){sleep(30);}else{
sleep(21600);
} //delete this else post debug
//spawn reboot
 pid_t pID = fork();
 if (pID == 0)                // child
 {
if(debug==1){printf("this is the debug output from spawned process);\n");} //"inline" debug
break; //abort loop in child- just in case if it is not interrupted already!
//
ckerror=system("killall tor");
//ckerror=system("killall polipo"); //uncomment if you want to reload polipo susbsystem as well. By default it's being left as-is as it looked completely stable on the long-run-tests
if( access( "/usr/tmp/torrc", F_OK ) != -1 ) {/*ALL IS WELL - NOP*/} else { 
 ckerror=system("tor-preinit");
// file doesn't exist
}//ALL IS /NOT/ WELL - RELOAD SYSTEMS

ckerror=system("tor --DataDirectory /tmp -f /usr/tmp/torrc");
if(ckerror==-1){printf("SYSTEM ALERT! Operation Attempt Failed :(\n"); return -1;}else{return 0; }

 }
	 
else if (pID < 0)            // failed to fork
{
		printf(" FORK SPAWN ERROR HAS OCCURRED!\n"); //C
		return -1;	       
}//end error-handle
else if(debug==1){ //if debugging tell parent who the child is [pid]
printf("PARENT HAS FINISHED SPAWNING THE CHILD WITH %d",pID);
}	 


}//end loop
return 0;  //mandatory master return
}//end main
